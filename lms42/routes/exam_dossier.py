from ..assignment import Assignment
from flask_login import current_user
from .. import utils
from ..app import db, app, get_base_url
from ..models import curriculum
from ..models.attempt import Attempt, get_notifications
from ..working_days import get_quarter_dates, get_working_hours_delta
import flask
from sqlalchemy import sql
import datetime
import pickle
import os
import glob


LMS_DIR = os.getcwd()


GRADES_QUERY = """
SELECT
    node_id,
    s.first_name,
    s.last_name,
    s.short_name,
    a.student_id,
    a.id AS attempt_id,
    a.number AS attempt_number,
    date(a.submit_time) AS date,
    status,
    g.grade,
    grader.short_name AS grader_name,
    consenter.short_name AS consenter_name,
    variant_id,
    start_time,
    submit_time,
    (
        SELECT COUNT(*)
        FROM attempt AS earlier
        WHERE earlier.node_id = a.node_id AND earlier.student_id = a.student_id AND earlier.number < a.number
    ) earlier_attempts,
    '*' as all
FROM attempt AS a
JOIN "user" s ON a.student_id = s.id
JOIN grading g ON g.id = (
        SELECT g2.id
        FROM grading g2
        WHERE g2.attempt_id = a.id
        ORDER BY g2.id desc
        LIMIT 1
)
LEFT JOIN "user" AS grader ON grader.id = g.grader_id
LEFT JOIN "user" AS consenter ON consenter.id = g.consent_user_id
WHERE a.node_id = :node_id AND status != 'ignored' AND date(submit_time) >= :start_date AND date(submit_time) < :end_date
ORDER BY node_id, submit_time
"""

ATTEMPT_COUNT_QUERY = """
SELECT node_id, COUNT(*) count
FROM attempt AS a
WHERE a.credits > 0 AND date(submit_time) >= :start_date AND date(submit_time) < :end_date
GROUP BY node_id
"""

FEEDBACK_QUERY = """
SELECT
    AVG(assignment_clarity) assignment_clarity,
    COUNT(assignment_clarity) assignment_clarity_ratings,
    AVG(difficulty) difficulty,
    COUNT(difficulty) difficulty_ratings
FROM node_feedback
WHERE date(time) >= :start_date AND date(time) < :end_date AND node_id = :node_id
"""

FEEDBACK_COMMENTS_QUERY = """
SELECT comments
FROM node_feedback
WHERE date(time) >= :start_date AND date(time) < :end_date AND node_id = :node_id AND comments IS NOT NULL AND comments != ''
"""


SPLITS = {
    "all": "All attempts",
    "earlier_attempts": "By number of earlier attempts",
    "variant_id": "By exam variant",
    "grader_name": "By primary examiner",
}


@app.route('/exam_dossier<period>', methods=['GET'])
@app.route('/exam_dossier', methods=['GET'])
@utils.role_required('inspector')
def exam_dossier(period=''):
    archive_mode = flask.request.cookies.get('archive')
    year, quarter, start_date, end_date = get_date_range(period)

    module_counts = {}
    for node in curriculum.get('tests'):
        module = curriculum.get('modules_by_id')[node["module_id"]]

        module_counts[node['id']] = {
            "name": module['name'],
            "link": f"/exam_dossier{period}/{node['id']}",
            "count": 0,
        }

    with db.engine.connect() as dbc:
        grades = dbc.execute(sql.text(ATTEMPT_COUNT_QUERY), start_date=start_date, end_date=end_date)
    for grade in grades:
        module_counts[grade['node_id']]["count"] = grade['count']
    module_counts = sorted(module_counts.values(), key=lambda x: -x["count"])

    return flask.render_template("dossier-index.html",
        archive_mode=archive_mode,
        session_cookie=flask.request.cookies.get('session'),
        base_url=get_base_url(),
        year=year,
        quarter=quarter,
        start_date=start_date,
        end_date=end_date,
        select_years=None if archive_mode else range(2020, datetime.date.today().year+1),
        module_counts=module_counts,
        outcomes_by_id=curriculum.get('outcomes_by_id'),
        endterms=curriculum.get('endterms'),
    )


@app.route('/exam_dossier/<node_id>/<int:attempt_id>', methods=['GET'])
@utils.role_required('teacher')
def exam_dossier_attempt(node_id, attempt_id):
    node = curriculum.get('nodes_by_id')[node_id]
    attempt = Attempt.query.get(attempt_id)
    assert attempt.node_id == node_id

    files = []
    pre = attempt.directory+"/"
    for file in glob.glob(pre+"**", recursive=True):
        if os.path.isfile(file):
            assert file.startswith(pre)
            short = file[len(pre):]
            if not short.startswith("screenshots/") and "/build/" not in short:
                files.append(short)

    ao = Assignment.load_from_directory(attempt.directory)
    return flask.render_template('node.html',
        topic=curriculum.get('modules_by_id')[node["module_id"]],
        student=attempt.student,
        node=node,
        assignments={
            "Attempt": {
                "html": ao.render("disabled", attempt.latest_grading, attempt),
                "notifications": get_notifications(attempt, attempt.latest_grading),
                "files": files,
            }
        },
        file_base=f"/exam_dossier/{node_id}/{attempt_id}/",
        base_url=f"{get_base_url()}/curriculum/{node_id}/static/",
        outcomes_by_id=curriculum.get('outcomes_by_id'),
        endterms=curriculum.get('endterms'),
        archive_mode=flask.request.cookies.get("archive"),
    )


@app.route('/exam_dossier/<node_id>/<int:attempt_id>/<path:file>', methods=['GET'])
@utils.role_required('teacher')
def exam_dossier_attempt_file(node_id, attempt_id, file):
    attempt = Attempt.query.get(attempt_id)
    assert attempt.node_id == node_id
    return flask.send_from_directory(LMS_DIR+"/"+attempt.directory, file, as_attachment=True, mimetype="application/octet-stream")


@app.route('/exam_dossier<period>/<node_id>', methods=['GET'])
@app.route('/exam_dossier/<node_id>', methods=['GET'])
@utils.role_required('inspector')
def exam_dossier_node(node_id, period=''):
    archive = flask.request.cookies.get('archive')
    year, quarter, start_date, end_date = get_date_range(period)

    node = curriculum.get('nodes_by_id')[node_id]
    module_id = node['module_id']
    module = curriculum.get('modules_by_id')[module_id]

    with db.engine.connect() as dbc:
        result = dbc.execute(sql.text(GRADES_QUERY), node_id=node_id, start_date=start_date, end_date=end_date)
        grades = [dict(grade) for grade in result]

    # Set `path` for each attempt
    for grade in grades:
        grade["path"] = f"/exam_dossier/{node_id}/{grade['attempt_id']}"

    # Calculate stats
    stats = {}
    times_spent = []
    for grade in grades:
        if grade["status"] not in ["passed", "failed"]:
            continue
        for split_prop, split_name in SPLITS.items():
            this_stats = stats.setdefault(split_name, {}).setdefault(grade[split_prop], {}).setdefault(grade["status"], {"count": 0, "total_grade": 0})
            this_stats["count"] += 1
            this_stats["total_grade"] += grade["grade"]
        # Calculate average time spent
        times_spent.append(get_working_hours_delta(grade["start_time"], grade["submit_time"]))

    for _, splits in stats.items():
        for variant, counts in splits.items():
            passed = counts.get("passed", {})
            failed = counts.get("failed", {})
            count = passed.get("count", 0) + failed.get("count", 0)
            splits[variant] = {
                "count": count,
                "passed_perc": str(round(passed.get("count", 0) / count * 100)) + "%",
                "passed_avg": passed.get("total_grade", 0) / passed.get("count", 1),
                "failed_avg": failed.get("total_grade", 0) / failed.get("count", 1),
            }

    # Filter only patches from this period
    variant_patches = get_exam_changes(node_id)
    if variant_patches:
        variant_patches = {variant: [patch for patch in patches if str(start_date) <= patch["date"] < str(end_date)] for variant, patches in variant_patches.items()}
        variant_patches = {variant: patches for variant, patches in variant_patches.items() if patches}
        # print("patches", module_id, variant_patches, str(start_date), str(end_date), flush=True)

    # Get student feedback
    with db.engine.connect() as dbc:
        feedback = dbc.execute(sql.text(FEEDBACK_QUERY), node_id=node_id, start_date=start_date, end_date=end_date).fetchone()
        feedback_comments = dbc.execute(sql.text(FEEDBACK_COMMENTS_QUERY), node_id=node_id, start_date=start_date, end_date=end_date).fetchall()


    return flask.render_template("dossier-module.html",
        node_name = module['name']+' · '+node['name'],
        year = year,
        quarter = quarter,
        select_years = None if archive else range(2020, datetime.date.today().year+1),
        current_link = f"/curriculum/{node_id}",
        grades = grades if current_user.is_teacher else None,
        start_date = start_date,
        end_date = end_date,
        stats = stats,
        variant_patches = variant_patches,
        avg_time = round(sum(times_spent)/len(times_spent),1) if times_spent else None,
        feedback = feedback,
        feedback_comments = feedback_comments,
        archive_mode = archive,
    )



change_cache_stat = None
change_cache_data = None

def get_exam_changes(node_id):
    global change_cache_stat
    global change_cache_data

    stat = os.stat('exam-changes.pickle')
    if change_cache_stat == None or stat.st_size != change_cache_stat.st_size or stat.st_mtime != change_cache_stat.st_mtime:
        change_cache_stat = stat
        with open('exam-changes.pickle', 'rb') as file:
            change_cache_data = pickle.load(file)

    return change_cache_data.get(node_id)


def get_date_range(period):
    year = None
    quarter = None
    parts = (period or '').replace('_', '').lower().split('q')
    if parts[0]:
        year = int(parts[0])
        if len(parts) > 1:
            quarter = int(parts[1])
            start_date, end_date = get_quarter_dates(year, quarter)
        else:
            start_date, _ = get_quarter_dates(year, 1)
            _, end_date = get_quarter_dates(year, 4)
        if end_date > datetime.date.today():
            end_date = datetime.date.today()
    else:
        start_date, _ = get_quarter_dates(2020, 3)
        end_date = datetime.date.today()
    return year, quarter, start_date, end_date
