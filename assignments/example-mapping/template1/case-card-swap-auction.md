# Card Swap Auction

You've been approached by a group of serious card collectors. Pokemon, Magic or football player cards are easily traded, but for those special cards they would like to reach out to a wider audience via the web. Instead of simply buying and selling cards they would like to continue to swap the cards. Luckily you known a good trusted third party delivery service that can take care of a physical exchange over distance that ensures everyone gets their cards. No need to worry about that. 

A collector should be able to put up a card (or set of cards) up for auction. Cards can only be part of one auction at a time. Other collectors have until a deadline to make a bid consisting of one or more cards. Bidders can change their bid during the auction. All collectors can see all the bids. When the deadline has passed and the auction has closed, the collector that made the offer must choose one of the bids and make the exchange. An offer must be chosen within 3 days otherwise an offer is selected at random. After an exchange was made libraries will be updated accordingly.

Cards used in a bid or auction are taken from the collectors library of cards. To add a card to the library it must be photographed on both sides in such a way that any defects are visible (high resolution, well lit and without filters). In addition name and type must be provided. Entering the card series is optional. Any (set of) card(s) can be swapped against any other (set of) card(s). So you need to allow users to indicate what types or series of cards they are interested in. You are asked to develop a website for desktop use.

## Example Mapping

Work on the app is split in these stories:

1. add card to your library
1. the auction


### Story: cards in your library

**Rule 1:** TODO: specify the rules, summarize the examples with a rule

**Example 1.1:** TODO: give examples that illustrate the rules

### Story: the auction

**Rule 1:** TODO: specify the rules, summarize the examples with a rule

**Example 1.1:** TODO: give examples that illustrate the rules

## *Questions*
Reflect on the rules and examples. Write down: 
    - Questions about whether the rules match the provided case description
    - Questions about what is missing in the provided case description
    - If there are any contradicting rules



## Datamodel

**todo: create a logical data model for the case above**

```plantuml
' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity entity {
    * attribute: text
}

' Add notes
note right of entity
    Notes
end note

'todo: complete the model

```


# Wireframes

**todo: create the wireframes using wiretext**

See: [case-card-swap-auction.wt](case-card-swap-auction.wt) OR [quant-ux](https://quant-ux.com)

If you use quant-ux past a sharing link to your prototype (main menu > share).

