
import datetime
import flask_wtf
import wtforms as wtf
from flask_wtf.file import FileAllowed, FileField
from wtforms import validators as wtv

class AddSafariForm(flask_wtf.FlaskForm):
    """Provides a form for adding a safari to the safari object

    For adding a safari into the database, make sure that they 'belong to' a
    Safari database object.
    """

    first_date = wtf.DateField('Main safari date', validators=[wtv.InputRequired()])
    second_date = wtf.DateField('Alternative safari', validators=[wtv.InputRequired()])
    company_signup_end_date = wtf.DateField('Company deadline', validators=[wtv.InputRequired()])
    student_signup_end_date = wtf.DateField('Student deadline', validators=[wtv.InputRequired()])

    submit = wtf.SubmitField('Submit')

    def validate(self, **kwargs):
        rv = flask_wtf.FlaskForm.validate(self)
        if not rv:
            return False
        
        today = datetime.datetime.now().date()

        if self.first_date.data <= today:
            self.first_date.errors.append('Main safari date has to be in the future.')
            return False

        if self.second_date.data <= today:
            self.second_date.errors.append('Alternative safari date has to be in the future.')
            return False

        if self.company_signup_end_date.data <= today:
            self.company_signup_end_date.errors.append('Company signup end date has to be in the future.')
            return False

        if self.student_signup_end_date.data <= today:
            self.student_signup_end_date.errors.append('Student signup end date has to be in the future.')
            return False

        if self.first_date.data < self.company_signup_end_date.data:
            self.company_signup_end_date.errors.append('Company sign up has to end before the first safari date.')
            return False

        if self.first_date.data < self.student_signup_end_date.data:
            self.student_signup_end_date.errors.append('Student sign up has to end before the first safari date.')
            return False

        if self.second_date.data is not None and self.second_date.data < self.company_signup_end_date.data:
            self.company_signup_end_date.errors.append('Company sign up has to end before the second safari date.')
            return False

        if self.second_date.data is not None and self.second_date.data < self.student_signup_end_date.data:
            self.student_signup_end_date.errors.append('Student sign up has to end before the second safari date.')
            return False

        if self.company_signup_end_date.data > self.student_signup_end_date.data:
            self.company_signup_end_date.errors.append('Company sign up has to end before student sign up starts.')
            return False

        return True


class AddSafariOfferForm(flask_wtf.FlaskForm):
    """Provides a form for a company to participate in a scheduled safari.

    For adding a safari company into the database, make sure that they 'belong to' a
    SafariOffer database object.
    """

    spots = wtf.IntegerField('How many students are you willing and able to accommodate? If you can\'t participate this time, just type 0 and submit.')
    use_alt_day = wtf.BooleanField('Check if you want to receive students on the alternative date instead of the main date.')

    contact_person = wtf.StringField('Who will be the students\' contact person?')
    contact_person_info = wtf.StringField('Phone number and/or email for this person.')

    extra_information = wtf.TextAreaField('Information for students. Time to arrive? Where/how to announce themselves? Should they bring lunch? Any special events? Anything else?')

    submit = wtf.SubmitField('Submit')
    def validate(self, **kwargs):
        rv = flask_wtf.FlaskForm.validate(self)
        if self.spots.data:
            for attr in ('contact_person', 'contact_person_info', 'extra_information'):
                if not getattr(self, attr).data.strip():
                    getattr(self, attr).errors.append('This field is required.')
                    rv = False
        return rv
    