const express = require('express');
const fs = require('fs');

// We're using the Better-SQLite3 NPM module as a database.
// Documentation: https://github.com/JoshuaWise/better-sqlite3/wiki/API
const Database = require('better-sqlite3');

// If you want to reset to a clean database, uncomment this line for a second or two:
//fs.unlinkSync('.data/sqlite3.db');
const db = new Database('sqlite3.db');

// Make sure tables and initial data exist in the database
db.exec(fs.readFileSync('schema.sql').toString());

// Create the subrouter, and set it as the default export
const router = express.Router();
module.exports = router;



/*
* The actual API routes...
*/

// Delay all request for 1s, to simulate latency:
router.use(function(req,rsp,next) {
    setTimeout(next, 1000);
});


router.get('/lists', function(req,rsp) {
    rsp.json(db.prepare('select * from lists').all());
});


router.post('/lists', function(req,rsp) {
    let name = req.body.name;
    if (typeof name !== 'string') {
        rsp.status(400).json({error: "Invalid name"});
    } else {
        let info = db.prepare('insert into lists(name) values(?)').run(name);
        let id = info.lastInsertRowid;
        rsp.status(201).json({id, name});
    }
});


function readList(listId,rsp) {
    let list = db.prepare('select * from lists where id=?').get(listId);
    if (list) return list;
    rsp.status(404).json({error: "No such list"});
}


router.get('/lists/:listId/items', function(req,rsp) {
    if (!readList(req.params.listId, rsp)) return;

    rsp.json(
        db.prepare('select * from items where listId=?')
        .all(req.params.listId)
        .map(itemFromDb)
    );
});


router.post('/lists/:listId/items', function(req,rsp) {
    if (!readList(req.params.listId, rsp)) return;
    
    let item = itemToDb(req.body, {checked: 0}, rsp);
    if (!item) return;
    
    item.listId = req.params.listId;
    
    let info = db.prepare('insert into items(listId,name,checked) values(:listId, :name, :checked)').run(item);
    item.id = info.lastInsertRowid;
    
    rsp.status(201).json(itemFromDb(item));
});


router.delete('/lists/:listId', function(req,rsp) {
    let info = db.prepare('delete from lists where id=?').run(req.params.listId);
    if (info.changes) {
        db.prepare('delete from items where listId=?').run(req.params.listId);
        rsp.status(200).json({});
    } else {
        rsp.status(404).json({error: "No such list"});
    }
});


router.delete('/lists/:listId/items/:itemId', function(req,rsp) {
    let info = db.prepare('delete from items where id=? and listId=?').run(req.params.itemId, req.params.listId);
    if (info.changes) rsp.status(200).json({}); // success!
    else rsp.status(404).json({error: "No such item"});
});


router.put('/lists/:listId/items/:itemId', function(req,rsp) {
    let item = db.prepare('select * from items where id=? and listId=?').get(req.params.itemId, req.params.listId);
    if (!item) {
        rsp.status(404).json({error: "No such item"});
        return;
    }
    
    item = itemToDb(req.body, item, rsp);
    if (!item) return;
    
    db.prepare('update items set name=:name, checked=:checked where id=:id').run(item);
    rsp.status(200).json(itemFromDb(item));
});


// Exceptions are internal errors
router.use(function(error,req,rsp,next) {
    rsp.status(500).json({error: 'Internal server error: '+error});        
});


// Catch-all rule for non-existing routes
router.use(function(req,rsp){
    rsp.status(404).json({error: "No such API route"});
});


/*
* A couple of helper functions...
*/



/** Converts a request `body` to an database item record, using `defaults` to fill
* in the blanks. If there's an error, it's send to `rsp` and `undefined` is return.
* Otherwise the item record object is returned.
*/
function itemToDb(body, defaults, rsp) {
    let item = Object.assign({}, defaults); // clone defaults
    
    if (body.name!=null) {
        item.name = body.name;
    }
    if (typeof item.name!=='string') {
        rsp.status(400).json({error: "Invalid name"});
    }
    
    if (body.checked===true) item.checked = 1;
    else if (body.checked===false) item.checked = 0;
    else if (body.checked!=null || item.checked==null) {
        rsp.status(400).json({error: "Invalid checked status"});
        return;
    }
    
    return item;
}


function itemFromDb(record) {
    return {
        id: record.id,
        name: record.name,
        checked: !!record.checked
    };
}

