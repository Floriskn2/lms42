# Chores

To help students with keeping their shared housing clean we want to provide them with a tool to track and share in the tasks involved. 

We'll make the following assumptions:
* they want to divide the work equally
* they are prone to forget to perform the task
* they need to do a chore every day to keep up with the work
* they will need a daily reminder
* slackers need more encouragement
* the same set of chores suffices for all houses
* the chores are split in such a way that they each require a similar amount of effort
* all chores need to be done once a week (shopping, cooking and doing the dishes will be supported in a different app)
* the week starts on monday
* chores are assigned at random
* if a student did not do a chore before the end of the week, then the chore carries over to the next week and the student needs to get pizza.


## Example Mapping

Work on the app is split in these stories:

1. add / remove housemates
2. divide chores equally
3. finishing chores

### Story: Add / Remove Housemates

**Rule 1:** a housemate can remove another housemate

**Example 1.1:** The one where you remove yourself

1. Bob, Alice and Mike are registered as housemates
1. Bob removes Bob
1. Bob is notified he cannot remove himself
1. Bob, Alice and Mike are registered as housemates

**Example 1.2** The one where you remove a housemate

1. Bob, Alice and Mike are registered as housemates
1. Bob removes Alice
1. Bob is notified that Alice has been removed
1. Bob and Mike are registered as housemates

**Rule 2:** a housemate can add a new housemate

**Example 2.1** The one where you register an existing name

1. Bob, Alice and Mike are registered as housemates
1. Bob adds Alice
1. Bob is notified that the name Alice is taken
1. Bob, Alice and Mike are registered as housemates

**Example 2.2** The one where you register a new housemate

1. Bob, Alice and Mike are registered as housemates
1. Bob adds Susan
1. Bob is notified that Susan has been added
1. Bob, Alice, Susan and Mike are registered as housemates

### Story: Divide Chores Equally

**Rule 3:** In turn a housemate is assigned a random chore until all chores have been assigned

**Example 3.1:** The one where the Bob starts in the assignment of chores
1. The list of chores is:

| chore               |
| ------------------- |
| vacuum the corridor |
| take out the trash  |
| mop the hallway     |
| clean the toilet    |
| mop the kitchen     |
1. Bob, Alice and Mike are registered as housemates
1. Assignment starts with Bob
1. It is monday
1. The personal lists of chores are:

| Bob                | Alice            | Mike                |
| ------------------ | ---------------- | ------------------- |
| take out the trash | clean the toilet | vacuum the corridor |
| mop the kitchen    | mop the hallway  |                     |

**Example 3.2** The one where Mike starts with the assignment of chores
1. The list of chores is:

| chore               |
| ------------------- |
| vacuum the corridor |
| take out the trash  |
| mop the hallway     |
| clean the toilet    |
| mop the kitchen     |
1. Bob, Alice and Mike are registered as housemates
1. Assignment starts with Mike
1. It is monday
1. The personal lists of chores are:

| Bob             | Alice               | Mike               |
| --------------- | ------------------- | ------------------ |
| mop the kitchen | vacuum the corridor | clean the toilet   |
| mop the hallway |                     | take out the trash |


**Rule 4:** The housemate where chore assignment starts alternates between housemate every week

**Example 4.1:** The one where Alice started last week

1. Bob, Alice and Mike are registered as housemates
1. Alice was first to be assigned a chore last week
1. Mike will be assigned a chore this week

**Rule 5:** Unfinished chores carry over to the next week

**Example 5.1** The one where Mike did not finish his chores

1. The list of chores is:

| chore                  |
| ---------------------- |
| vacuum the corridor    |
| take out the trash     |
| mop the hallway        |
| clean the toilet       |
| mop the kitchen        |
| vacuum the living room |
| clean the bathroom     |
1. Bob, Alice and Mike are registered as housemates
1. Assignment starts with Alice
1. It is monday
1. The personal lists of chores before assignment are:

| Bob | Alice | Mike             |
| --- | ----- | ---------------- |
|     |       | clean the toilet |
|     |       |                  |
1. The personal lists of chores after assignment are:

| Bob             | Alice               | Mike                   |
| --------------- | ------------------- | ---------------------- |
| mop the kitchen | vacuum the corridor | clean the toilet       |
| mop the hallway | clean the bathroom  | take out the trash     |
|                 |                     | vacuum the living room |


### Story: Finishing Chores

**Rule 6:** When a housemate marks a chore as completed, it is removed from their list of chores for that week

**Example 6.1:** The one where Alice completes a chore
1. Alice has to "vacuum the corridor", "take out the trash", "mop the hallway"
1. Alice marks "vacuum the corridor" as complete
1. Alice has to "take out the trash", "mop the hallway" 

**Rule 7:** Housemates are reminded every day to perform chores

**Example 7.1:** The one where nothing is left to do
1. Alice has to do nothing
1. It is one o'clock in the afternoon
1. Alice does not receive a reminder to do her chores

 **Example 7.2:** The one where Alice is reminded
1. Alice has to "vacuum the corridor"
1. It is one o'clock in the afternoon
1. Alice receives a reminder: "Please remember to do your chores"

**Rule 8:** If a student has more than twice the number of chores left than there are days left in the week, then the daily reminder will be more urgent

**Example 8.1:** The one where Bob does his chores (2 days left, 4 chores)
1. On saturday Bob has to "mop the hallway", "take out the trash", "clean the toilet", "mop the kitchen"
1. It is one o'clock in the afternoon
1. Bob receives a reminder: "Please remember to do your chores"

**Example 8.2:** The one where Bob is slacking (2 days left, 5 chores)
1. On saturday Bob has to "vacuum the corridor", "take out the trash", "mop the hallway", "clean the toilet", "mop the kitchen"
1. It is one o'clock in the afternoon
1. Bob receives a reminder: "Bob, you lazy bastard, do your chores"

## Datamodel

**todo: create a logical data model for the case above**

```plantuml
' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity entity {
    * attribute: text
}

' Add notes
note right of entity
    Notes
end note

'todo: complete the model

```


# Wireframes

**todo: create the wireframes using wiretext or quant-ux**

See: [case-chores.wt](case-chores.wt) OR [quant-ux](https://quant-ux.com)

If you use quant-ux past a sharing link to your prototype (main menu > share).
