(function() {
    if (self.fillNotGraded) return;

    self.fillNotGraded = function(element) {
        let formE = element;
        while(formE.tagName !== 'FORM') formE = formE.parentNode;

        for(let taE of formE.getElementsByTagName('textarea')) {
            if (taE.value=='') taE.value = "Not graded";
        }

        for(let zeroE of formE.querySelectorAll('input[type="radio"][value="0"], input[type="radio"][value="no"]')) {
            if (!formE.querySelector(`input[type="radio"][name="${zeroE.name}"]:checked`)) {
                // No checked item in this radio group get
                zeroE.checked = true;
            }
        }

        for(let selE of formE.querySelectorAll('select[name="formative_action"]')) {
            selE.value = 'repair';
        }

        formE.dispatchEvent(new Event('input', {bubbles: true})); // cause scores to update
    };

    self.fillAllGood = function(element) {
        let formE = element;
        while(formE.tagName !== 'FORM') formE = formE.parentNode;

        for(let taE of formE.getElementsByTagName('textarea')) {
            if (taE.value=='Not graded') taE.value = "";
        }

        for(let zeroE of formE.querySelectorAll('input[type="radio"][value="3"], input[type="radio"][value="yes"]')) {
            if (!formE.querySelector(`input[type="radio"][name="${zeroE.name}"]:checked`)) {
                // No checked item in this radio group get
                zeroE.checked = true;
            }
        }

        for(let selE of formE.querySelectorAll('select[name="formative_action"]')) {
            selE.value = 'passed';
        }

        formE.dispatchEvent(new Event('input', {bubbles: true})); // cause scores to update
    };
})();
