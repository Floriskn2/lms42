from __future__ import annotations
from flask_login import login_required
import flask

from .. import utils
from ..app import app, db, csrf
from ..models.ai import AiGrading, AiGradingPrompter, examine_attempt
from ..models.attempt import Attempt
from .api import token_required


# In your browser, login as a teacher/inspector and then open:
#   $BASE_URL/api/authorize?host=ai&token=89mG2rc5qZds
# On the command line run (the httpie command) to upload my_prompter.py:
#   http $BASE_URL/ai/grading_prompter Authorization:89mG2rc5qZds @my_prompter.py

@app.route('/ai/grading_prompter', methods=['POST'])
@token_required
@csrf.exempt
def set_ai_grading_prompter():
    user = flask.request.user
    if not user.is_inspector:
        return flask.jsonify({"error": "Only inspectors can set the prompter."}, 403)
    
    python_code = flask.request.data.decode()
    agp = AiGradingPrompter(python_code=python_code, user_id=flask.request.user.id)
    db.session.add(agp)
    db.session.commit()
    return flask.jsonify({"version": agp.id})


@app.route('/ai/gradings', methods=['POST'])
@utils.role_required('inspector')
def perform_ai_grading():
    attempt_id = flask.request.form.get("attempt_id")
    attempt = Attempt.query.get(attempt_id)
    if not attempt:
        return "No such attempt.", 404

    ai_grading = examine_attempt(attempt_id)
    if not ai_grading:
        return "Prompter doesn't want this attempt graded.", 403
    return flask.redirect(f"/ai/gradings/{ai_grading.id}")


@app.route('/ai/gradings', methods=['GET'])
@utils.role_required('inspector')
def last_ai_graded():
    ai_graded = AiGrading.query.order_by(AiGrading.id.desc()).limit(500).all()
    return flask.render_template('ai-grading-list.html', ai_graded=ai_graded)


@app.route('/ai/gradings/<int:ai_grading_id>', methods=['GET'])
@utils.role_required('inspector')
def ai_grade(ai_grading_id):
    ai_grading = AiGrading.query.get(ai_grading_id)
    objectives_feedback = feedback_markdown(ai_grading)
    return flask.render_template('ai-grading-attempt.html', ai_grade=ai_grading, objectives_feedback=objectives_feedback)


def feedback_markdown(ai_grading):
    if not ai_grading.output:
        return "No output."
    output_object = ai_grading.output_object
    latest_grading = ai_grading.attempt.latest_grading
    motivations = latest_grading.objective_motivations if latest_grading else []
    
    objectives_count = max(len(output_object), len(motivations))

    result_markdown = ""
    for i in range(objectives_count):
        obj = f"Objective #{i + 1}"
        result_markdown += f"### {obj}\n\n"
        ai_feedback_array = output_object.get(obj, {}).get("feedback")
        motivation = motivations[i] if i < len(motivations) else ""
        
        for feedback in ai_feedback_array:
            objective_feedback = feedback
            if objective_feedback in motivation:
                motivation = motivation.replace(f"- {objective_feedback}", "")
                result_markdown += f"- <span style='color:green'>{objective_feedback}</span>\n"
            else:
                result_markdown += f"- {objective_feedback}\n"

        if motivation.lstrip():
            motivation_array = motivation.split("-")
            for motivation_item in motivation_array:
                if motivation_item.strip():
                    result_markdown += f"- <span style='color:orange'>{motivation_item.strip()}</span>\n"

    return result_markdown
