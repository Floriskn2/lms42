import saxy_tree
import traceback
import sys
from typing import NoReturn


class Parser:
    def __init__(self, tokens):
        self.__tokens = tokens
        self.__current_token_pos = 0
        self.__expected_tokens = [] # The list of things the current token did *not* match (used for error reporting).

    @property
    def token(self):
        """Get the current token. This is a @property, so use `self.token` instead of `self.token()`."""
        return self.__tokens[self.__current_token_pos]

    def next_token(self):
        """Progress to the next token."""
        self.__expected_tokens = []
        self.__current_token_pos += 1

    def add_expected_token(self, token_description):
        if token_description not in self.__expected_tokens:
            self.__expected_tokens.append(token_description)

    def match_kind(self, kind):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `kind`, or None otherwise.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        if self.token.kind == kind:
            token = self.token
            self.next_token()
            return token
        self.add_expected_token(f"<{kind}>")
        return None

    def match_text(self, text):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directly. It returns the current token *if* it matches the given `text`, or None otherwise.
        `text` may also be a list of options to match.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the kind is added to `expected_tokens`, which helps `error` give
        a better message."""
        texts = text if isinstance(text, list) else [text]
        if self.token.text in texts:
            token = self.token
            self.next_token()
            return token
        for text in texts:  # noqa: PLR1704
            self.add_expected_token(repr(text))
        return None

    def error(self) -> NoReturn:
        """Display an error message, indicating the line, column, text and kind of the current token and
        the list of tokens that were expected instead (from `self.expected_tokens`), and exits the program.
        """
        traceback.print_stack(file=sys.stdout)
        message = f"Expected {' or '.join(self.__expected_tokens)}, but found {repr(self.token)}."
        print(f"\nERROR: {message} at line {self.token.line} column {self.token.column}.", file=sys.stderr)
        sys.exit(1)

    def require(self, val):
        """A utility function that may be convenient instead of checking for errors manually. It
        exits with an error if `val` is `None` or `False`, or just returns `val` otherwise.
        """
        if val is False or val is None:
            self.error()
        return val

    def parse(self):
        """The main parse method. It returns a Program object, or exits with an error.
        It should be called only once on an instance.

        <Program> ::= <Block> EOF
        """
        expressions = self.parse_block()
        # Make sure that after parsing all expressions that we can, we have arrived at the
        # end-of-file marker. (If not, that would indicate code at the end.)
        self.require(self.match_kind("eof"))
        return saxy_tree.Program(expressions)

    def parse_eol(self):
        if self.match_kind("eol"):
            while self.match_kind("eol"):
                pass
            return True
        return False

    def parse_block(self):
        """<Block> ::= <Expression> (EOL+ <Expression>)* EOL*"""
        self.parse_eol()
        expressions: list[saxy_tree.Expression] = [
            self.require(self.parse_expression())
        ]
        while True:
            if not self.parse_eol():
                break
            expression = self.parse_expression()
            if not expression:
                break
            expressions.append(expression)
        # We have reached the end of the expressions.
        return saxy_tree.Block(expressions)

    # TODO: Parse expression and other parsing methods.
    def parse_expression(self):
        pass
