/*
 * If-else
 */

// An if statement with a then-block containing multiple statements;
if (1) {
    // This happens only when the condition is true (which it is, as 1 is trueish).
    var z = "A string";

    // Within the conditional block, we're nesting another if. This one also has
    // an else-block (which is optional). 
    if (0) {
        // Empty block, won't be executed because a 0 equals FALSE.
    } 
    else {
        var something;
    }
}
