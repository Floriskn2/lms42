const net = require('net');

const PORT = 6666;

function send(socket, message) {
    console.log(`Sending:/n${message}`);
    socket.write(`${message}\n`);
}

const server = net.createServer((socket) => {
    socket.setEncoding("utf8");

    console.log("Client connected to server");

    socket.on("data", function(data) {
        // Data has been received on the socket
        const lines = data.trim().split("\n");
        for (const line of lines) {
            console.log(`Received: ${line}`);
            send(socket, data);
        }
    });

    socket.on("end" ,() => {
        // Connection ended
        console.log("Client disconnected from server");
    });
});

server.listen(PORT);