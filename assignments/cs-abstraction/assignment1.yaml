Introduction:
    - |
        An important concept of object-oriented programming is abstraction. Abstraction, in the context of OOP, refers to the ability to hide complex implementation details and show only the necessary features of an object. This simplifies the interaction with objects, making programming more intuitive and efficient.
        Aside from Abstraction, we go into several other concepts here such as interfaces ans static classes and factory's.
    -
        link: https://www.w3schools.com/cs/cs_abstract.php
        title: Abstraction
        info: This article explains the basics of abstraction.

    -
        link: https://www.w3schools.com/cs/cs_interface.php
        title: Interfaces
        info: This article explains the basics of interfaces.

    -
        link: https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/static-classes-and-static-class-members
        title: Static Classes and Static Class Members
        info: This article explains the basics of statics

    - |
        ## Static attributes and methods in class diagrams
        Static attributes (instance variables) and methods can also be represented in class diagrams, using an underline:

        ```plantuml
        @startuml
        class SerialNumber {
            -{static} nextNumber: int
            -number: int
            -{static} GetNextNumber(): int
            +GetNumber(): int
        }
        ```

    - |
        ## Abstract classes in class diagrams
        Class diagrams also allow classes and methods to be abstract, indicated by an **A** instead of a **C** next to the class name, and the italic font used in the method name.

        ```plantuml
        abstract class Shape {
            +{abstract} Draw()
        }
        class Circle {
            +Draw()
        }
        Shape <|-- Circle
        ```

Tic Tac Toe Pro:
    - |
        You've built Tic Tac Toe before. Let's do it again, only this time we'll be adding quite a few features:

        - Choice between a simple and a fancy (multi-colored) board.
        - Two types of player bots to choose from, besides human players.
        - Configurable board size.

        The end result should work something like this:

        Try to imagine what (even worse) mess your code would become if you were to add all of these features to your original, non-object-oriented implementation. 😨

        OOP to the rescue!

        Your final program at the end of this assignment should have a Class diagram similar to the following diagram:

        ```plantuml
        package TicTacToe.Application {
            class Game {
                +Game(Player p1, Player p2, Board board)
                +PlayTurn()
                +GameRunning: bool
                +LastMove: (int, char)
                +Winner: Player
            }

            Game o--> Board
            Game o--> "2" Player

            class Board {
                +Board(size: int, boardPrinter: IBoardPrinter)
                +Size: int
                +SetMarker(square: int, marker: char)
                +GetMarker(square: int): char
                +GetWinnerMarker() : char
                +CheckDraw(): bool
                +Display()
            }

            interface IBoardPrinter {
                +PrintBoard(board: Board)
            }

            abstract class Player {
                +Name: string
                +Marker: char
                {abstract} +ChooseSquare(board: Board): int
            }

            class HumanPlayer extends Player {
                +HumanPlayer(marker: char, name: string, IPlayerInputProvider inputProvider)
                +ChooseSquare(board: Board): int
            }

            interface IPlayerInputProvider {
                +ChooseSquare(board: Board, Player: player)
            }

            class RandomPlayer extends Player {
                +ChooseSquare(board: Board): int
            }

            class SequentialPlayer extends Player {
                +ChooseSquare(board: Board): int
            }
        }

        package TicTacToe.UI {    
            class SimpleBoardPrinter extends IBoardPrinter {
                +PrintBoard(board: Board)
            }

            class FancyBoardPrinter extends IBoardPrinter {
                +PrintBoard(board: Board)
            }

            class PlayerInputProvider extends IPlayerInputProvider {
                +ChooseSquare(board: Board, Player: player)
            }

            static class PlayerFactory(){
                {static} +GetPlayers()
            }

            static class BoardFactory(){
                {static} +GetBoard()
            }

            class Program(){
                {static} -Main()
            }
        }

        @enduml
        ```

    - Board:
        -   text: |
                Create a `Board` class. It should have at least...

                - A method call for setting and getting `X`es and `O`s to/from a given square.
                - A method that prints the current state of the board, using simple monochrome output. You can use the following symbols for the borders: `╚ ╗ ╦ ╩ ╬ ║ ═`
                - A method that returns the winner's symbol (`X` or `O`) if there is a winner.
                - A method to check if there's a draw (the board is full).

                Create a small test program (or even better, a proper *unit test*, if you know how), to make sure that your board behaves as it should before moving on to the next objective.
            ^merge: feature
            map:
                abstraction: 1

    - The basic game:
        -   text: |
                Create a basic working version of the game, by implementing a (human) `Player` and the `Game` class.

                The `Player` class should...

                - Ask the player for his/her name in the constructor. However, there's a rule: each player can only play once while your program is running. If, in a subsequent game (during the same program execution) or for the second player in the same game, the same name is entered, the program should respond with `Denied! You have already played.` and ask again. Use a *static* attribute for this.
                - Hold the player's token (`X` or `O`).
                - Have a method that gets a `Board` object as an argument, asks the user which square he/she chooses, and returns the coordinates of that square.

                The `Game` class should...

                - Ask the user for the desired board size.
                - Instantiate the board and two players.
                - Have a method that plays the game, alternating player turns until there is a winner or the board is full.

                You should now be able to play human-vs-human matches displayed in the simple monochrome format, with user-configured board sizes.
            ^merge: feature
            map:
                abstraction: 1
                static: 1

    - Computer players:
        -   text: |
                Split up the `Player` class into two parts: an *abstract* `Player` class and a `HumanPlayer` class that inherits from the former. In addition, implement two player bots:

                - A random player, the picks random available squares. Random players should automatically pick a random name.
                - A sequential player, that always picks the first available square. Sequential players should automatically get a name containing a sequential number, so `Seq1`, `Seq2`, etc.

                Use polymorphism to have a player choose a square in the way that is appropriate for that player type.

                In the `Game` class, ask the user what player types he/she desires. To prevent code duplication and to prevent the constructor from becoming too long, you should use a `static` method for this.
            ^merge: feature
            map:
                abstraction: 1
                static: 1

    - Multi Project:
        -   text: |
                Create a new project TicTacToe.UI and give it a project reference to TicTacToe.Application. This project is used for all 'UI' related questions and should be the only project that references the console.

                In order to achieve this implement the following:
                - A SimpleBoard printer class that is able to print the board in simple monochrome output that gets used by the board. Depend on an interface in the Board class.
                - Similar, implement a PlayerInputProvider that handles the player choosing a square. The HumanPlayer should depend on an interface.
                - Implement factories for creating the player and board objects from the console.
                - Last, chain everything together in the Program class
                - Don't forget to make everything private and/or internal whenever possible. You should leak as few implementation details as possible in your TicTacToe.Application class.
            ^merge: feature
            map:
                abstraction: 1
                static: 1            
    - 
        link: https://spectreconsole.net/
        title: Spectre.Console
        info: Spectre.Console Docs.

    - Fancy board:
        text: |
            Create another implementation "FancyBoardPrinter" for the IBoardPrinter interface. This one should display a multi-colored board using the `SpectreConsole` library. It doesn't have to be very fancy, as long as it's working differently.
            On startup, have the user select the type of board.

            *You'll need to install `SpectreConsole` using NuGet with `dotnet add package Spectre.Console` from the project folder. 
        ^merge: generic