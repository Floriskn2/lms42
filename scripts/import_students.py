import csv
import random

from lms42.app import db, app
from lms42.models.user import User


with open('inschrijvers.csv') as file:
    reader = csv.DictReader(file)
    for row in reader:
        # ,Achternaam,Voorvoegsel,Voorletters,Roepnaam,Klas
        first = row['Roepnaam'].strip()
        last = row['Achternaam'].strip()
        if row['Voorvoegsel'].strip():
            last = row['Voorvoegsel'].strip() + ' ' + last

        clss = row['Klas'].strip().lower()
        if clss == '-':
            continue

        if clss == 'a':
            clss = 'ESD1V.a'
            slb = User.query.filter_by(short_name='joost').first()
        elif clss == 'b':
            clss = 'ESD1V.b'
            slb = User.query.filter_by(short_name=random.choice(['frank','tibor'])).first()
        elif clss == 'd':
            clss = 'DSD1V.a'
            slb = User.query.filter_by(short_name='remkob').first()
        else:
            print("wrong class "+clss)
            continue

        user = User.query.filter_by(first_name=first, last_name=last).first()
        if user:
            user.is_hidden = False
            user.is_active = True
            db.session.commit()
            continue

        short_name = first.lower().replace(' ', '')
        if User.query.filter_by(short_name=short_name).first():
            short_name = short_name + last[0].lower()
            
        email = row['Email privé '].lower().strip()

        print(short_name, first, last, email, clss, slb.first_name)
        user = User(short_name=short_name, first_name=first, last_name=last, email=email, class_name=clss, counselor_id=slb.id)

        db.session.add(user)
        db.session.commit()

        
        
        

