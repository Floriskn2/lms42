def test_register_existing_internship(client):
    client.login("teacher1")
    client.post("/companies/add", data = {
        "name": "Example company"
    })

    client.login("student1")

    client.open("/internship/register")
    client.find("input", value="Save").submit(company_id="1", start_date="2024-03-26", end_date="2024-05-18")

    client.find("a", text="Edit").require(1)
    client.find("h2", text="Student1").require(1)
    client.find("p", text="No review yet...").require(1)


def test_register_new_internship(client):
    client.login("student1")

    client.open("/internship/register")
    client.find("input", value="Save").submit(company_id="-1", start_date="2024-03-26", end_date="2024-05-18", company_name="test company", contactperson_first_name="John", contactperson_last_name="Doe", contactperson_email="johndoe@example.com")

    client.find("a", text="Edit").require(1)
    client.find("h2", text="Student1").require(1)
    client.find("p", text="No review yet...").require(1)
    client.find("td", text="John Doe").require(1)
    client.find("a", text="test company").require(1)
    client.find("div", text="test company").require(1)


def test_edit_review(client):
    client.login("teacher1")
    client.post("/companies/add", data = {
        "name": "Example company"
    })

    client.login("student1")

    client.open("/internship/register")
    client.find("input", value="Save").submit(company_id="3", start_date="2024-03-26", end_date="2024-05-18")

    client.open("/companies/internship/3/update")
    client.find("input", value="Save").submit(review="This is a test review.")

    client.find("p", text="This is a test review.").require(1)


def test_delete_internship(client):
    client.login("teacher1")
    client.post("/companies/add", data = {
        "name": "Example company"
    })

    client.login("student1")

    client.open("/internship/register")
    client.find("input", value="Save").submit(company_id="4", start_date="2024-03-26", end_date="2024-05-18")

    client.login("admin")
    client.open("/companies/4")

    client.find("input", value="Delete").require(1)
    client.find("input", value="Delete").submit()

    client.find("a", text="Edit").require(0)
    client.find("h2", text="Student1").require(0)
    client.find("p", text="No review yet...").require(0)
