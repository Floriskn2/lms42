- |
  We're creating a simple text-based role playing game, which we'll be extending in the next lesson. A `Game` class has already been provided in the template. We recommend that you study it and use it for inspiration.

-
  ^merge: feature
  title: Create the Player class
  weight: 5
  text: |
    Create a class called `Player` that has the following methods:

    - `__init__`: Asks the user for his/her name and stores this as the `name` attribute. It also sets the `hp` (health points) attribute to `INITIAL_HEALTH`.
    - `play_turn`: Receive a reference to a `Game` object as an argument. Uses the provided `ask_choice` function to ask the user what he/she would like to use with a prompt like this: "*Peter, what will you do?*", where *Peter* should of course be the name for this player. Currently, there should be two choices: *heal* and *attack*. When the user selects *heal*, the `heal` method should be called. When the user selects *attack* the `attack` method should be called, passing a reference to the `Game` object as an argument.
    - `heal`: Modifies `hp` such that the player restores health halfway up to `INITIAL_HEALTH` (which is 100). So if `hp` was 60, it should become 80. `hp` should always increase by at least 5 though, even if the user was already at (almost) full health. So if `hp` was 98 it should become 103 (instead of 99). It should also print something like *Peter is starting to feel better: 103 hp.*
    - `is_dead`: Returns `True` if the `hp` attribute is 0 (the player is dead) or `False` otherwise.
    - `sustain_damage`: Receives an integer specifying the amount of damage as an argument. It subtracts this amount from the `hp` attribute, making sure it never goes below 0. It prints either something like *Peter suffered 25 damage.* or *Peter died.*, in case the player is dead (making use of the `is_dead` method).
    - `__str__`: Special method called by Python when it wants to represent an object as a string. Search the [Object-Oriented Programming (OOP) in Python 3](https://realpython.com/python3-object-oriented-programming/) document for `__str__` if you don't know what this is about. This method should return something like *Peter (100 hp)* or *Peter (DEAD)* when the player `is_dead()`.
    - `attack`: Receives a reference to a `Game` object as an argument. Uses the `ask_choice` function (passing in all `game.players` except the current player as choices), to ask the user *Peter, which player would you like to attack?*. Calls `sustain_damage` on the selected player, passing a random value between 0 and 50 as the damage argument.

    Remember that all Python methods, by definition, receive `self` as their first argument.

    You can test your `Player` class while you're writing it by adding code like this to the bottom of the file:

    ```python
    game = Game()
    my_player = game.players[0]
    my_player.sustain_damage(50) 
    my_player.heal() 
    my_player.play_turn(game)
    print("Dead?", my_player.is_dead())
    print(f"The output for __str__ is: {my_player}")
    # etc
    ```

    Make sure all methods work as intended before moving on.

-
  ^merge: feature
  title: Implement the declare_winner method
  weight: 2
  text: |
    Provide the implementation for the `declare_winner` method of the provided `Game` class.
    Its intended behavior is provided as inline documentation.

-
  ^merge: feature
  title: Run the game
  weight: 0.5
  text: |
    Actually start the game when you run your Python file, by creating a `Game` instance and calling the `run` method on it. You should preferably do this within a `if __name__ == '__main__':` condition at the bottom of your code.

    Make sure the game runs as expected.
