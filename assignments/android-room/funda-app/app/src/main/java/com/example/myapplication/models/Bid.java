package com.example.myapplication.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Bid {
    @PrimaryKey(autoGenerate = true)
    public long id;

    public String name;
    public int amount;

    public long houseId;
}
