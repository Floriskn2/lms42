name: REST and test
description: Design a tidy REST API, implement it using Express and test it with Cypress.
days: 2
pair: true
goals:
    rest: 1
    nodejs: 1
    apitests: 1
assignment:
    Introduction:
        - |
            For this assignment, you will design, implement and test a REST API for a craft beer social network (similar to Untappd, if you happen to be a craft beer enthusiast).
            
            The objectives will break this task down for you. The database schema (`schema.sql`) and all queries you'll need (`data.js`) have been provided.

    REST APIs:
        -
            link: https://www.youtube.com/watch?v=7YcW25PHnAA
            title: REST API concepts and examples
        -
            link: https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
            title: Best practices for REST API design
            info: When designing your craft beer API, you should take all of the advice given on this page into account, *except* the parts about pagination, SSL (you *would* use `https` in production of course, but it's impractical in the context of an assignment) and caching (as it isn't quite clear what would need to be cached and for how long). The article also contains Node.js/Express code examples.
        -
            link: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            title: Wikipedia - List of HTTP status codes
            info: The list of HTTP status codes. You'll probably want to use at least 200, 201, 401, 403 and 404 in this assignment.
        -
            link: https://httpie.io/docs/cli/usage
            title: HTTPie - Documentation - Usage & Examples
            info: HTTPie is a nice and simple command-line HTTP client, useful for playing around with (REST) APIs. You can install it as `httpie` using *Add/remove software*.

        - |
            Here are some quick examples of simple REST API calls and their responses:

            | Verb | Path | Request body | Response code | Response body |
            | ---- | ---- | ------------ | ------------- | ------------- |
            | GET | /movies | | 200 | [{"id": 1, "title": "The Matrix", …}, …] |
            | GET | /movies?genre=drama | | 200 | [{"id": 123, "title": "Titanic", …}, …] |
            | GET | /movies/123 | | 200 | {"id": 123, "title": "Titanic", …} |
            | PUT | /movies/123 | {"rating": 3} | 200 | {"id": 123, "rating": 3, …} |
            | POST | /movies | {"title": "Hackers", …} | 201 | {"id": 1337, "title": …} |
            | POST | movies/123/actors | {"name": "Brad Pitt"} | 201 | {"id": 67, …} |
            | DELETE | /movies/123 | | 204 | {} |
            | GET | /users/frankvv/favorite_movies | | 200 | [{"id": 1, "title": "The Matrix"}, …] |
            | DELETE | /users/frankvv/favorite_movies/123 | | 204 | {} |

            Requests and responses that carry a body would have their `Content-Type` HTTP header set to `application/json`.

        -
            ^merge: feature
            text: |
                Create a proper REST API that allows a user to…

                - Add a beer to the system.
                - Remove a beer. If the beer doesn't exist, an error should be returned.
                - Get a list of all beers. Optionally the API user should be able to filter it by brewery, category or by minimum or maximum alcohol percentage. And the API user should be able to sort

                - Get a list of all users. No need for filtering nor ordering.
                - Add a user. The password should be passed in plain text, but hashed (using bcrypt of course) before it's inserted into the database. An error should be returned if a user with the given name already exists.

                - Get the list of *liked* beers for a user with a given user name. 
                - Add a *like*, given a beer id and a user name. All errors should be handled appropriately.
                - Remove a *like*, given a beer id and a user name. All errors should be handled appropriately.

                Implement your API starting with `server.js`, splitting up into multiple files if things get unwieldy. (You can optionally use [Express Router](https://www.tutorialspoint.com/expressjs/expressjs_routing.htm) for a cleaner separation.) You can just use `data.js` to interact with the database - no need to write any queries yourself.

                For this objective, you can completely ignore authentication and authorization: all operations are permitted by everyone on the network. We'll get to this later.

                You can use HTTPie to manually test your code. (No need to overdo this, as we'll be adding automated testing next.) Make sure you send JSON data (the default for HTTPie), and not HTML form data.


    Cypress API testing:
        - |
            Cypress is a test framework that is mostly meant to be used for end-to-end tests. It works by automating an actual browser engine (Chrome) to simulate clicks (and other events) and to verify that the resulting HTML is as expected. We'll be using it for that purpose later on.

            Cypress can also be used for API tests though, which has the advantage that you don't need to learn two separate test framework, each with its own way of doing things.
        -
            link: https://www.testingwithmarie.com/post/api-testing-with-cypress
            title: Marie Drake - API Testing with Cypress
            info: |
                This article explains why and how to use Cypress for API tests.
        -
            link: https://devhints.io/chai.html
            title: Chai.js cheatsheet
            info: Cypress includes the Chai.js library, which provides you with functions that you can use to determine if a test should fail or succeed. It has two styles, *assert* and *expect* (BDD) - you can use either one.
        -
            ^merge: feature
            text: |
                Fully test the API you built using Cypress API tests. Don't forget to test the error conditions as well. You'll want to split your tests into multiple files.

                The testing has already been setup and an example has been provided in `cypress/integration/example.js`. The provided `package.json` is configured to run the tests using just:

                ```sh
                npm test
                ```

                We have configured Cypress to reset the database to `schema.sql` before running each test.

                **Important:** You should not only check for the correct responses to requests, but also verify that the database was actually updated as a result of `POST`, `UPDATE` and `DELETE` requests. You can do that by combining two requests (say a `POST` and a `GET`) in a single test case.

    Authentication and authorization:
        -
            link: https://www.youtube.com/watch?v=UBUNrFtufWo
            title: Session vs Token Authentication in 100 Seconds
            info: |
                In *1.2 Dynamic Web* you learned to use sessions for authentication and authorization. APIs usually employ a different mechanism: (JWT) tokens.
        -
            link: https://jwt.io/introduction
            title: Introduction to JSON Web Tokens
            info: This article explains JWT in more detail. You don't need to sweat the details, but make sure you grasp the basic ideas.
        -
            link: https://www.npmjs.com/package/jsonwebtoken
            title: NPM repository - jsonwebtoken
            info: |
                The documentation for the most commonly used Node.js JWT library.
        -
            link: https://www.tutorialspoint.com/expressjs/expressjs_middleware.htm
            title: TutorialsPoint - ExpressJS - Middleware
            info: If you want to perform the same action for each (or many) of your routes, such as checking authentication, Express middleware is an easy way to do that.
        -
            ^merge: feature
            text: |
                Add a route to your API that allows users to exchange a valid user name and password for a JWT access token. The access token should include user's name and level.

                Create an Express *middleware* function that does authentication. It should check if a request has a JWT token and if it is valid, and then set the trusted `userName` and `userLevel` on the `request` object, such that these can be used for authorization by each of the later routes.

                Next, add authorization to your other API routes as follows:
                
                - Only users with level 8 and up should be able to:
                    - Add new users.
                    - Add new beers.
                    - Delete beers.
                - Only the user who it concerns should be able to:
                    - Add a *like*.
                    - Remove a *like*.
                - Only users (of any level) should be able to:
                    - Get a list of all users.
                    - Get the list of *likes* for any specific user.
                - Anybody, with or without specifying a login token, should be able to:
                    - Get the list of beers.
                    - And obviously, obtain an access token by providing a valid user name and password.

                This will break your tests for now.


    Auth testing:
        -
            link: https://docs.cypress.io/api/cypress-api/custom-commands
            title: Cypress documentation - Custom commands
            info:
        -
            ^merge: feature
            text: |
                Modify and extend your tests to factor in authentication and authorization.
                
                Many of the tests will need to do the exact same thing: log in. To prevent a lot of code repetition and test slowness, we have provided the mostly finished skeleton for a custom command in `cypress/support/index.js`. You just need to complete it in order to work with your API, and you can than use it in many of your tests.

                The idea of this custom Cypress function is that tests can call `cy.userRequest` instead of `cy.request`, the only difference being that the former accepts a username (and password) to authenticate the request with as its first parameter. For example, the following would try to log in with username `admin` and password `admin`, and then send a `GET` request to `/some_resource` using the token received by the login request:

                ```python
                cy.userRequest("admin:admin", {url: "/some_resource"})
                ```

                Make sure you don't just test the happy path (where access should be granted) but the unhappy paths as well (where access should be denied for some reason).
