import blessed
import random

terminal = blessed.Terminal()

# Clear the screen and move to the top left.
print(terminal.home + terminal.normal + terminal.clear, end="")

# Print the letters A-Z in random colors.
for x in range(26):
    color_code = random.choice([
        terminal.black_on_lawngreen,
        terminal.black_on_magenta3,
        terminal.white_on_gray10,
        terminal.white_on_gray20,
    ])
    char = chr(ord('A') + x)

    print(color_code + char, end="")
